/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 08:56:14 by bbichero          #+#    #+#             */
/*   Updated: 2016/09/24 13:12:56 by nidzik           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include    "../include/scop.h"
#include <OpenGL/gl.h>
#include "/nfs/2014/n/nidzik/.brew/Cellar/glfw3/3.2.1/include/GLFW/glfw3.h"
 
int 		loop_hook(t_scop *scop)
{

	float points[] = {
		0.0f,  0.5f,  0.0f,
		0.5f, -0.5f,  0.0f,
		-0.5f, -0.5f,  0.0f
	};


//init buffer and fill it with tab of flat here 
	GLuint vbo = 0;
	glGenBuffers (1, &vbo);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glBufferData (GL_ARRAY_BUFFER, 9 * sizeof (float), points, GL_STATIC_DRAW);

//init VertexArray
	GLuint vao = 0;
	glGenVertexArraysAPPLE (1, &vao);
	glBindVertexArrayAPPLE (vao);
	glEnableVertexAttribArray (0);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

const char* vertex_shader =
"#version 400\n"
"in vec3 vp;"
"void main () {"
"  gl_Position = vec4 (vp, 1.0);"
	"}";

const char* fragment_shader =
"#version 400\n"
"out vec4 frag_colour;"
"void main () {"
"  frag_colour = vec4 (0.5, 0.0, 0.5, 1.0);"
	"}";

GLuint vs = glCreateShader (GL_VERTEX_SHADER);
glShaderSource (vs, 1, &vertex_shader, NULL);
glCompileShader (vs);

GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
glShaderSource (fs, 1, &fragment_shader, NULL);
glCompileShader (fs);

GLuint shader_programme = glCreateProgram ();
glAttachShader (shader_programme, fs);
glAttachShader (shader_programme, vs);
glLinkProgram (shader_programme);

// wipe the drawing surface clear
glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
glUseProgram (shader_programme);
glBindVertexArrayAPPLE (vao);
// draw points 0-3 from the currently bound VAO with current in-use shader
glDrawArrays (GL_TRIANGLES, 0, 3);
// update other events like input handling 
glfwPollEvents ();
// put the stuff we've been drawing onto the display


glfwSwapBuffers(WIN);
///mlx_opengl_swap_buffers(WIN);




    return (0);
}

/*
 * Initialise mlx openGL windows, loop and event
 */
t_scop		*ft_init_window(t_scop *scop)
{
	if (!glfwInit())
		ft_putstr("error init");
	else
	{
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	WIN = glfwCreateWindow(WIN_X, WIN_Y, "Scop", NULL, NULL);
//	if (!WIN)
//		return (1);
	glfwMakeContextCurrent(WIN);
	glfwSetInputMode(WIN, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(WIN, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwPollEvents();
	glfwSetCursorPos(WIN, WIN_X / 2.0, WIN_Y / 2.0);
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	}
	while (!glfwWindowShouldClose(WIN))
	{
		loop_hook(scop);
}



	glfwTerminate();
	ft_putstr("coucou");


	return (scop);
}

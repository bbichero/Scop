/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 09:26:07 by bbichero          #+#    #+#             */
/*   Updated: 2016/09/25 11:10:11 by nidzik           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/scop.h"

int             main(int ac, char **av)
{
    t_scop     *scop;
	t_parse		parse;

    scop = malloc(sizeof(scop));
    (void)ac;
	parse = get_file(av);    
    //ft_init_scop(scop);
//    scop = ft_init_window(scop);
	free(scop);
	free(&parse);
    return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 13:02:41 by bbichero          #+#    #+#             */
/*   Updated: 2015/03/10 10:52:05 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_strcmp(const char *s1, const char *s2)
{
	int		i;

	i = 0;
	if (s1 == NULL && s2 == NULL)
		return (0);
	if (s2 == NULL)
		return (-1);
	if (s1 == NULL)
		return (1);
	while (s1[i] != '\0' && s1[i] == s2[i] && s2[i] != '\0')
		i++;
	if (s1[i] == '\200' && s2[i] == '\0')
		return (s2[i] - s1[i]);
	return (s1[i] - s2[i]);
}

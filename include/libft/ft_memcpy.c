/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 11:42:12 by bbichero          #+#    #+#             */
/*   Updated: 2014/11/11 16:14:32 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			*ft_memcpy(void *dest, const void *src, size_t n)
{
	char		*d;
	char		*s;

	if (n == 0 || dest == src)
		return (dest);
	d = (char *)dest;
	s = (char *)src;
	while (--n)
		*d++ = *s++;
	*d = *s;
	return (dest);
}

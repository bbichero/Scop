/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: romontei <romontei@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 18:06:23 by romontei          #+#    #+#             */
/*   Updated: 2015/03/15 11:33:05 by romontei         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char *stock;

	stock = ft_strnew(20);
	if (stock && n >= 0)
	{
		*--stock = '0' + (n % 10);
		n = n / 10;
		while (n != 0)
		{
			*--stock = '0' + (n % 10);
			n = n / 10;
		}
	}
	else if (stock != NULL)
	{
		*--stock = '0' - (n % 10);
		n = n / 10;
		while (n != 0)
		{
			*--stock = '0' - (n % 10);
			n = n / 10;
		}
		*--stock = '-';
	}
	return (stock);
}

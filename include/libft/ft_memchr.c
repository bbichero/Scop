/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/11 13:02:06 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:50:06 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*src;
	unsigned int	i;

	i = 0;
	src = (unsigned char *)s;
	if (s && n > 0)
	{
		while (i < n)
		{
			if (src[i] == (unsigned char)c)
				return ((void *)(src + i));
			i++;
		}
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:10:35 by bbichero          #+#    #+#             */
/*   Updated: 2014/11/11 10:19:16 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	int		i;
	int		j;
	int		p;

	p = n;
	i = 0;
	j = 0;
	if (s2[j] == '\0')
		return ((char *)s1);
	while (p > 0 && s1[i] != '\0')
	{
		if (s1[i] == s2[j])
			j++;
		else
		{
			i = i - j;
			j = 0;
			p = n - i;
		}
		p--;
		i++;
		if (s2[j] == '\0')
			return ((char *)&s1[i - j]);
	}
	return (0);
}

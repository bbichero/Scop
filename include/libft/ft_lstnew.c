/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/12 10:06:04 by bbichero          #+#    #+#             */
/*   Updated: 2015/03/16 12:48:02 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*newlist;

	newlist = (t_list *)malloc(sizeof(t_list));
	if (newlist == NULL)
		return (NULL);
	newlist->next = NULL;
	if (content == NULL)
	{
		newlist->content = NULL;
		newlist->content_size = 0;
		return (newlist);
	}
	newlist->content = (void *)malloc(content_size);
	if (newlist->content == NULL)
		return (NULL);
	ft_memcpy((newlist->content), content, content_size);
	newlist->content_size = content_size;
	newlist->next = NULL;
	return (newlist);
}

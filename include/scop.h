/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 08:42:46 by bbichero          #+#    #+#             */
/*   Updated: 2016/09/24 12:13:05 by nidzik           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

/* Include all system lib use */
# include   <math.h>
# include   <stdlib.h>
# include   <unistd.h>
# include   <stdio.h>
# include   <stdlib.h>
# include   <OpenGL/gl.h>

/* Include all personal lib use */
# include   "../source/minilibx_macos/mlx.h"
# include   "../source/minilibx_macos/mlx_opengl.h"
# include   "./libft/libft.h"
# include 	"parser.h"

/* Define varible often use */
# define WIN		scop->win
# define MLX		scop->mlx
# define WIN_X		480
# define WIN_Y		320

/* t_scop structure with mlx variable, ...  */
typedef struct      s_scop
{
    void            *win;
    void            *mlx;

    // This will identify our vertex buffer
    GLchar          *vertex_shader;
    GLchar          *fragment_shader;
}                   t_scop;

t_scop             *ft_init_window(t_scop *scop);
int         		loop_hook(t_scop *scop);
//t_scop             ft_init_scop(t_scop *scop);

#endif

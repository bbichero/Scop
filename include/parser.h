/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nidzik <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/24 11:30:47 by nidzik            #+#    #+#             */
/*   Updated: 2016/09/25 12:27:24 by nidzik           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

/* Include all system lib use */
# include   <math.h>
# include   <stdlib.h>
# include   <unistd.h>
# include   <stdio.h>
# include   <stdlib.h>
# include 	<fcntl.h>

/* Include all personal lib use */
# include   "./libft/libft.h"

/* Define varible often use */
# define VERT		parse.tabvertex
# define VERTP		parse->tabvertex
# define FACE		parse.tabface
# define FACEP		parse->tabface
# define FD			parse.fd
# define FDP		parse->fd
# define SIZEV		parse.size_vertex
# define SIZEVP		parse->size_vertex
# define CHECKP		parse->check
# define LP			*line == '.'

typedef enum { false, true } bool;

/* t_parse structure with parser utils var , ...  */

typedef struct      s_parse
{
	float 	*tabvertex;
	int 	*tabface;
	int		fd;
	int 	n;
	int size_vertex;
	bool check;
}                   t_parse;



t_parse *ft_strjoinfloat(t_parse *parse, float *line);
t_parse get_file(char** tab);
t_parse *ifv(char *line, t_parse *parse);
t_parse get_file(char** tab);
float 	*getfloatingpoint(char *line);
float ft_atof(char *number, int i);
void ft_ptf(t_parse parse);
int *getintpoint(char *line);


#endif

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbichero <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/19 08:43:13 by bbichero          #+#    #+#              #
#    Updated: 2016/09/24 12:11:21 by nidzik           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = scop

# LIBFT
LFTPATH = include/libft/
LFTIPATH = -I $(LFTPATH)
LFT = -L $(LFTPATH) -lft

# MLX
MLXPATH = source/minilibx_macos/
MLXIPATH = -I $(MLXPATH)
MLX = -L $(MLXPATH) -lft

OBJPATH = obj
SRCPATH = source
INCLUDE = -I include/

# MLX
CC = gcc
#GRAPHLIB = -L/usr/X11/lib -lmlx -lXext -lX11 -framework openGl -framework AppKit
GRAPHLIB = -framework openGl -L/nfs/2014/n/nidzik/.brew/Cellar/glfw3/3.2.1/lib -lglfw3 -framework AppKit


LIBS = $(LFT) $(GRAPHLIB)
INCLUDES = $(INCLUDE)


BASEFLAGS = -Wall -Wextra
CFLAGS = $(BASEFLAGS) -Werror -O2 -g


LFTCALL = all
LFTRE = re


SRCSFILES = main.c mlx_init.c parse.c parser.c

SRC = $(addprefix $(SRCPATH)/,$(SRCSFILES))
OBJECTS = $(SRC:$(SRCPATH)/%.c=$(OBJPATH)/%.o)

RM = rm -rf

Y = \033[0;33m
R = \033[0;31m
G = \033[0;32m
E = \033[39m

all: l  $(NAME)

$(NAME): $(OBJECTS)
	@echo "$(Y)[COMPILING SCOP] $(G) $(CC) -o $@ $(CFLAGS) objs.o $(LIBS) $(E)"
	@$(CC) -o $@ $(CFLAGS) -g $(OBJECTS) $(INCLUDES) $(LIBS)
	@echo "$(Y)[COMPILING SCOP DONE]$(E)"

$(OBJECTS): $(OBJPATH)/%.o : $(SRCPATH)/%.c
	@mkdir -p $(dir $@)
	$(CC) -o $@ $(CFLAGS) $(INCLUDES) -c $<

clean:
	$(RM) $(OBJPATH)

fclean: clean
	$(RM) $(NAME)

l:
	@echo "$(Y)[COMPILING LIBFT] $(G) make -C $(LFTPATH) $(LFTCALL) $(E)"
	@make -C $(LFTPATH) $(LFTCALL)
	@echo "$(Y)[COMPILING LIBFT DONE]$(E)"

#libmlx:
#	@echo "$(Y)[COMPILING MLX] $(G) make -C $(MLXPATH) $(LFTCALL) $(E)"
#	@make -C $(MLXPATH) $(LFTCALL)
#	@echo "$(Y)[COMPILING MLX DONE]$(E)"

re: fclean all
